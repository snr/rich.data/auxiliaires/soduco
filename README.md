# Travail sur la base de données SODUCO

## [Atelier](./atelier)

Le code et la chaîne de traitement utilisés pour l'atelier de liage d'entités nommées organisé par Soduco. 

## [Séminaire](./seminaire)

Le code et les requêtes préparatoires pour la présentation du projet Richelieu au séminaire Soduco, à partir
des données de Soduco.

