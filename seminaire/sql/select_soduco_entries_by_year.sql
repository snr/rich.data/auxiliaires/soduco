-- nombre d'entrées de bottin par années de publication
SELECT e.published AS directory_date, COUNT(e) AS entry_count
FROM directories.elements AS e
GROUP BY e.published
ORDER BY e.published;