from pandas.core.frame import DataFrame as DFType
from pandas.core.series import Series as SType
from plotly import graph_objects as go
from plotly.graph_objs._figure import Figure as FigureType
from typing import List, Dict
from math import ceil, floor
import pandas as pd
import numpy as np
import csv
import ast
import os


# ********************************************
#                                    CONSTANTS
# ********************************************
# directories
IN = os.path.join(os.path.abspath(""), "in")
OUT = os.path.join(os.path.abspath(""), "out")
OUTDATA = os.path.join(OUT, "data")
OUTPLOT = os.path.join(OUT, "plot")
# plotly stuff
COLORS = {
    "white": "#ffffff", "cream": "#fcdcd9", "blue": "#0000ef", 
    "burgundy": "#890c0c", "pink": "#ff94c9", "gold": "#da9902", 
    "lightgreen": "#8fc7b1", "darkgreen": "#00553e", "peach": "#f9b55c",
    "lilac": "#a592fc"
}
LAYOUT_BASE = {
    "paper_bgcolor": COLORS["white"],
    "plot_bgcolor": COLORS["cream"],
    "margin": {"l": 50, "r": 50, "t": 50, "b": 50},
    "showlegend": True,
    "xaxis": {"anchor": "x", "title": {"text": r"$\text{Décennie}$"}},
    "grid": {"pattern": "coupled"}
}
COLS_COLORS = { "architectes": COLORS["peach"], "nouveautes": COLORS["pink"], 
                "chapeliers": COLORS["lilac"] }
DS_COLORS = { "Soduco": COLORS["lilac"], "Richelieu": COLORS["gold"] }
LINE = { "width": 2, "color": COLORS["burgundy"] }





# ********************************************
#                                    FUNCTIONS
# ********************************************
def int2decade(date:int) -> int:
    """
    round a 4-digit integer into a decade.
    the decade is represented as the middle of 
    the decade to simplify sorting:
    `1938` -> `1935`, `1872` -> `1875`
    
    :param date: the date to process
    """
    # confirm that the date is valid
    assert isinstance(date, int) and date > 999 and date < 9999 \
           , ValueError(f"* invalid value for date to decade conversion: { date }")
    
    # exact decade: 1910...
    if date/10 == date//10:
        return date - 5
    # not an exact decate: 1919...
    else:
        return int( ( (floor(date/10)*10) + (ceil(date/10)*10) ) / 2 )
    return
    
    
def list2decade(date:List[str]) -> List[int]|float:
    """
    extract an array of decade(s) out of a date range
    expressed as an array: `[ <min date>: <max date> ]`. 
    the list contains as many decade(s) as are present in
    the range: `[ 1842, 1872 ]` will be changed into 
    `[ 1845, 1855, 1865, 1875 ]`
    """
    if len(date) != 2:
        print(f"invalid date range: `{ date }`")
        return np.nan
    else: 
        _ = date
        date = sorted([ int(d) for d in date ])                  # retype to `int` necessary: the date is sometimes expressed as a string
        date = [ int2decade(date[0]), int2decade(date[1]) ]      # upper / lower decades
        decadecount = round(date[1]/10) - round(date[0]/10) - 1  # number of decades between lower and upper dates in the range
        date = sorted(list(set( [ date[0] ]                      # extract decades. the `+ [date[1]]` at the end is sometimes necessary but can create duplicates => deduplicate the list
                                + [ date[0] + (d+1)*10 for d in range(decadecount) ] 
                                + [ date[1] ] )))
        return date
    return


def build_df_r(df_r:DFType, df_icon:DFType, col:str) -> DFType:
    """
    build `df_r` by counting occurences of themes by decade in `df_icon`
    
    the themes chosen to compare between `df_icon` and the data from soduco are:
    * `architecture` for `architectes`                                                  , 147 times present in the `df_icon` dataset
    * `boutique` and `commerce` for `nouveautés`                                        , 136 times present in the `df_icon` dataset
    * `mode`, `tailleur`, `chapelier`, `bottier`, `costume`, `parfumeur` for `chapelier`, 66 times present in `df_icon`
    
    :param df_r: the richelieu dataframe that is being built with this function
    :param df_icon: the dataframe containing iconographical data from the richelieu project
    :param col: the column which we're working on: `chapelier`, `nouveautes` or `architecte`
    """
    keywords = {
        "chapeliers" : [ "mode", "tailleur", "chapelier", "bottier", "costume", "parfumeur" ],
        "nouveautes" : [ "boutique", "commerce" ],
        "architectes": [ "architecture" ]
    }  # { <column name>: <themes to count in `df_icon`> }
    
    for d in df_r.decade:
        df_r.loc[ (df_r.decade==d), col] = (df_icon.loc[ (df_icon.decade==d) 
                                                          & df_icon.theme.isin(keywords[col]) ]
                                                   .shape[0])
    return df_r
    

def build_df_s(df_s:DFType, df_data:DFType, col:str) -> DFType:
    """
    build `df_s` by counting occurences of `chapeliers`, 
    `nouveautés`, `architectes` per decade in the dataframes
    created from the soduco database.
    
    :param df_s: the soduco dataframe being built with this function
    :param df_data: a dataframe created from a query on the soduco database for a 
                    profession (`df_chap`, `df_arch` or `df_nouv`)
    :param co:: the column we're working on: `chapelier`, `nouveautes` or `architecte`
    """
    for d in df_s.decade:
        df_s.loc[ (df_s.decade==d), col ] = df_data.loc[ df_data.decade==d ].shape[0]
    return df_s


def build_figure(
    df:DFType
    , yaxis_title:str
    , fig_title:str
    , scatterplot:bool
) -> FigureType:
    """
    create a plotly figure on the columns 
    "chapeliers", "nouveautes", "architectes"
    of the different dataframes
    
    :param df: the dataframe to process
    :param yaxis_title: the title of the Y axis
    :param fig_title: the title of the figure
    :param scatterplot: create a filled scatterplot instead of a group bar chart
    :returns: the plotly figure object
    """
    layout = LAYOUT_BASE.copy()                           # layout of the figure, completed below
    cols = [ "chapeliers", "nouveautes", "architectes" ]  # column names to process in the df
    
    if scatterplot: 
        cols = [ s[0] for s in                            # order columns by average number of values
                 sorted([ [c, df[c].mean()] for c in cols ]
                        , key=lambda x: x[1]
                        , reverse=False) ]
        data = [ go.Scatter(x=df.repr
                            , y=df[c]
                            , fillcolor=COLS_COLORS[c]
                            , line=LINE
                            , stackgroup="one"
                            , orientation="v"
                            , name=c) 
                 for c in cols ]
    
    else:
        data = [ go.Bar(x=df.repr
                        , y=df[c]
                        , marker={ "color": COLS_COLORS[c], "line": LINE }
                        , name=c)
                 for c in cols ]
        layout["barmode"] = "group"

    layout["yaxis"] = { "anchor": "x", "title": { "text": r"$\text{ %s }$" % yaxis_title } }
    layout["title"] = r"$\text{ %s }$" % fig_title
    
    return go.Figure(data=data, layout=layout)


def build_figure_comparison(dsdf:Dict, col:str) -> FigureType:
    """
    build a plotly figure comparing the frequence of 
    two professions in the two datasets for each decade, 
    in base 1 1835
    
    :param dsdf: a dict mapping a dataset name to a df: 
                 `{ "Soduco": df_r_base, "Richelieu": df_s_base }`
    :param col: the column / profession to process. 
                one of `"chapeliers", "architectes" or "nouveautes"`
    """
    assert col in ["chapeliers", "architectes", "nouveautes"] \
           , f"build_figure_comparison(): got `{col}` for arg col, "\
             + "expected one of ['chapeliers', 'architectes', 'nouveautes']"

    layout = LAYOUT_BASE.copy()
    data = [
        go.Scatter(x=df.repr
                   , y=df[col]
                   , line={ "width": 4, "color": DS_COLORS[ds] }
                   , marker={ "color": COLORS["burgundy"], "line": LINE }
                   , mode="lines+markers"
                   , name=ds)
        for ds, df in dsdf.items()
    ]
    layout["yaxis"] = { "anchor": "x", "title": { "text": r"$\text{ %s (base 1 1831-1835, à taille de corpus égale) }$" % col } }
    layout["title"] = r"$\text{ Évolution de la présence de %s dans les jeux de données Soduco et Richelieu }$" % col
    fig = go.Figure(data=data, layout=layout)    

    # we need to `udpdate_xaxes` because plotly has trouble ordering 
    # `repr` columns => we explicitly reorder columns
    return fig.update_xaxes(categoryorder="array",
                            categoryarray=dsdf["Richelieu"].sort_values("decade").repr.to_list())





# ********************************************
#                                     PIPELINE
# ********************************************
def pipeline():
    """
    processing pipeline
    """
    # create output dirs
    [ os.makedirs(d, exist_ok=True) for d in [OUT, OUTDATA, OUTPLOT] ]
    
    # 1) variables
    # input dfs
    df_s_count= pd.read_csv(os.path.join(IN, "select_soduco_entries_by_year_out.csv"))
    df_nouv = pd.read_csv(os.path.join(IN, "select_viviennebourse_nouveautes_out.csv"), index_col="index")
    df_chap = pd.read_csv(os.path.join(IN, "select_viviennebourse_chapelier_out.csv"), index_col="index")
    df_arch = pd.read_csv(os.path.join(IN, "select_viviennebourse_architecte_out.csv"), index_col="index")
    df_icon = pd.read_csv(os.path.join(IN, "richelieu_icono.csv"), sep="\t", index_col="index")
    
    # output dfs. 
    # `df_s` = soduco dataframe; `df_r` = richelieu dataframe
    # * `decade`      : the decade for which data is counted
    # * `repr`        : a string representation of the decade: `1901-1910`
    # * `total`       : for `df_r`, the total number of entries for that decade. for `df_s`, the sum of entries in the database for `chapeliers`, `nouveautes` and `architectes`
    # * `chapeliers`  : number of entries for "nouveautés" for that decade
    # * `architectes` : number of entries for "architectes" for that decade
    # * `nouveautes`  : number of entries for "chapeliers" for that decade
    df_s = pd.DataFrame(columns=["decade", "repr", "total", "chapeliers", "nouveautes", "architectes"])
    df_r = pd.DataFrame(columns=["decade", "repr", "total", "chapeliers", "nouveautes", "architectes"])

    # 2) retypes and filtering to keep only relevant data
    df_icon.theme = df_icon.theme.str.lower().apply(ast.literal_eval)
    df_icon = df_icon.loc[ df_icon.theme.apply(lambda x: len(x) > 0)
                           & df_icon.date.notna() ]
    df_icon.date = df_icon.date.apply(ast.literal_eval)
    
    # 3) quick analysis 
    # a. count themes in `df_icon`
    # get all unique themes from the iconography dataset + the number of occurences.
    # this is useful to select which themes to count. print it and write to csv
    themes = [ i for x in df_icon.theme.to_list() for i in x ]       # all themes in `df_icon`, with duplicates
    themesdedup = sorted(set(themes))                                # all unique themes
    themecounter = [ [t, themes.count(t)] for t in themesdedup ]     # list of `[ <theme>, <number of occurences> ]`
    themecounter = sorted(themecounter, key=lambda inner: inner[1])  # sort by number of occurences of the theme
    themecounter = [["theme", "nombre d'occurences"]] + themecounter # add headers
    maxlen = len(max(themesdedup, key=len))                          # length of the longest string in themesdedup
    print("\n".join( f"{ t[0] }{ '.' * (maxlen - len(t[0]) + 2) }{ t[1] }" for t in themecounter))
    with open(os.path.join(OUTDATA, "richelieu_theme_counter.csv"), mode="w") as fh:
        writer = csv.writer(fh, delimiter="\t")
        [ writer.writerow(t) for t in themecounter ]
        
    # b. count number of entries per decade in the whole soduco database
    df_s_count["decade"] = df_s_count.directory_date.apply(int2decade)
    df_s_decade = pd.DataFrame(columns=["decade", "repr", "entry_count"])
    df_s_decade.decade = sorted(df_s_count.decade.unique())
    df_s_decade = (pd.merge(df_s_decade                                         # empty df with decades
                            , df_s_count.groupby(["decade"]).entry_count.sum()  # number of entries by decade
                            , on="decade")
                     .rename(columns={"entry_count_y": "entry_count"})
                     .drop(columns=["entry_count_x"]))
    df_s_decade["repr"] = df_s_decade.decade.apply(lambda x: f"{x-4}-{x+5}")
    
    # 4) create `df_r` and `df_s`, which contain stats in absolute numbers for each decade
    # the `decade` columns will contain each decade in which the entries are created, as lists
    # a ressource can be created as a single date, or within a range of dates => the lists will
    # all the decades within the date ranges. a decade is represented by its middle date:
    # `1855` will be used for the decade `1851-1960`.
    df_nouv["decade"] = np.nan
    df_chap["decade"] = np.nan
    df_arch["decade"] = np.nan
    df_icon["decade"] = np.nan
    df_nouv["decade"] = df_nouv.published.apply(int2decade).apply(lambda x: [x] if not pd.isna(x) else x)
    df_arch["decade"] = df_arch.published.apply(int2decade).apply(lambda x: [x] if not pd.isna(x) else x)
    df_chap["decade"] = df_chap.published.apply(int2decade).apply(lambda x: [x] if not pd.isna(x) else x)    
    df_icon["decade"] = df_icon.date.apply(list2decade)
    
    # build `df_r`
    df_icon = df_icon.explode("theme").explode("decade")                             # `.explode()` splits a row containing a list into several rows
    df_r.decade = df_icon.decade.drop_duplicates().dropna().sort_values().to_list()  # all decades in `df_icon` are added to `df_r.decades`
    df_r.repr = df_r.decade.apply(lambda x: f"{x-4}-{x+5}")                          # string repr of the decade
    df_r = build_df_r(df_r, df_icon, "nouveautes")
    df_r = build_df_r(df_r, df_icon, "chapeliers")
    df_r = build_df_r(df_r, df_icon, "architectes")
    for d in df_r.decade:
        df_r.loc[ df_r.decade==d, "total" ] = df_icon.loc[ (df_icon.decade==d) ].shape[0]
    
    # build `df_s`
    df_chap = df_chap.explode("decade")
    df_nouv = df_nouv.explode("decade")
    df_arch = df_arch.explode("decade")
    df_s.decade = sorted(set( df_chap.decade.dropna().to_list()
                              + df_arch.decade.dropna().to_list()
                              + df_nouv.decade.dropna().to_list() ))
    df_s.repr = df_s.decade.apply(lambda x: f"{x-4}-{x+5}")
    df_s = build_df_s(df_s, df_arch, "architectes")
    df_s = build_df_s(df_s, df_nouv, "nouveautes")
    df_s = build_df_s(df_s, df_chap, "chapeliers")
    df_s.total = df_s["chapeliers"] + df_s["architectes"] + df_s["nouveautes"]
    
    # write the dfs to file. 
    df_s.to_csv(os.path.join(OUTDATA, "soduco_absolu.csv")
                , sep="\t", encoding="utf-8", index_label="index")
    df_r.to_csv(os.path.join(OUTDATA, "richelieu_absolu.csv")
                , sep="\t", encoding="utf-8", index_label="index")
    
    # 5) express `df_s` and `df_r` in relative numbers to make comparisons
    cols = [ "total", "chapeliers", "nouveautes", "architectes" ]  # the columns which we'll recalculate
    # first, build `df_s_frac` and `df_r_frac`.
    # the dataframes represent the counts in proportions per decade: 
    # all 3 columns are expressed as fractions of `total` for each decade
    # so that, `chapeliers` + `nouveautes` + `architectes` == 1
    df_s_frac = df_s.copy()
    df_r_frac = df_r.copy()
    df_r_frac.total = df_r_frac.chapeliers + df_r_frac.nouveautes + df_r_frac.architectes
    s_denominator = df_s_frac.total.copy()  # numbers to divide `df_s_frac` columns by
    r_denominator = df_r_frac.total.copy()  # numbers to divide `df_r_frac` cols by
    df_s_frac[cols] = df_s_frac[cols].divide(s_denominator, axis=0)
    df_r_frac[cols] = df_r_frac[cols].divide(r_denominator, axis=0)
    
    # then, build `df_s_base` and `df_r_base`.
    # each column of the dataframe is represented in base 1 for the decade 1835.
    cols = [ "chapeliers", "nouveautes", "architectes" ]
    df_s_base = df_s_frac.copy()
    df_r_base = df_r_frac.copy()
    r_denominator = df_r_frac.loc[ df_r_frac.decade==1835, cols ].squeeze()
    s_denominator = df_s_frac.loc[ df_s_frac.decade==1835, cols ].squeeze()
    df_s_base[cols] = df_s_base[cols].divide(s_denominator, axis=1)
    df_r_base[cols] = df_r_base[cols].divide(r_denominator, axis=1)
    df_r_base.total = df_r_base[cols].sum(axis=1)
    df_s_base.total = df_s_base[cols].sum(axis=1)
    
    # print and write to file
    print("df_r", "\n", df_r, "\n\n"
          , "df_s", "\n", df_s, "\n\n"
          , "df_r_frac", "\n", df_r_frac, "\n\n"
          , "df_s_frac", "\n", df_s_frac, "\n\n"
          , "df_r_base", "\n", df_r_base, "\n\n"
          , "df_s_base", "\n", df_s_base, "\n\n"
          , "df_s_decade", "\n", df_s_decade, "\n\n")
    csvparams = { "sep": "\t", "encoding": "utf-8", "index_label": "index" }
    df_r_frac.to_csv(os.path.join(OUTDATA, "richelieu_fraction.csv"), **csvparams)
    df_s_frac.to_csv(os.path.join(OUTDATA, "soduco_fraction.csv"), **csvparams)
    df_r_base.to_csv(os.path.join(OUTDATA, "richelieu_base_1835.csv"), **csvparams)
    df_s_base.to_csv(os.path.join(OUTDATA, "soduco_base_1835.csv"), **csvparams)
    df_s_decade.to_csv(os.path.join(OUTDATA, "soduco_nb_entrees_decade.csv"), **csvparams)
    
    # 6) plots
    cols = [ "chapeliers", "nouveautes", "architectes" ]
    
    # represent all dataframes as graphs
    fig_r = build_figure(df_r
                         , yaxis_title="Chapeliers, nouveautés, architectes  (nombres absolus)"
                         , fig_title="Nombre de chapeliers, nouveautés et architectes dans le corpus Richelieu}$"
                         , scatterplot=False)
    fig_s = build_figure(df_s
                         , yaxis_title="Chapeliers, nouveautés, architectes  (nombres absolus)"
                         , fig_title="Nombre de chapeliers, nouveautés et architectes dans le corpus Soduco}$"
                         , scatterplot=False)
    fig_r_frac = build_figure(df_r_frac
                              , yaxis_title="Chapeliers, nouveautés, architectes (en proportion relative au total des trois professions)"
                              , fig_title="Répartition relative des chapeliers, nouveautés et architectes dans le corpus Richelieu"
                              , scatterplot=True)
    fig_s_frac = build_figure(df_s_frac
                              , yaxis_title="Chapeliers, nouveautés, architectes (en proportion relative au total des trois professions)"
                              , fig_title="Répartition relative des chapeliers, nouveautés et architectes dans le corpus Soduco"
                              , scatterplot=True)
    fig_r_base = build_figure(df_r_base
                              , yaxis_title="Chapeliers, nouveautés, architectes (en base 1 1831-1840, à taille de corpus égale)"
                              , fig_title="Évolution du nombre de chapeliers, nouveautés et architectes dans le corpus Richelieu"
                              , scatterplot=False)
    fig_s_base = build_figure(df_s_base
                              , yaxis_title="Chapeliers, nouveautés, architectes (en base 1 1831-1840, à taille de corpus égale)"
                              , fig_title="Évolution du nombre de chapeliers, nouveautés et architectes dans le corpus Soduco"
                              , scatterplot=False)
    
    # number of entries in the soduco database per decade
    fig_s_decade = go.Figure(
        data=go.Bar(x=df_s_decade.repr
                    , y=df_s_decade.entry_count
                    , marker={ "color": COLORS["pink"], "line": LINE }
                    , name="Nombre d'entrées d'annuaires"),
        layout=dict({ "yaxis": { "anchor": "x", "title": { "text": r"$\text{Nombre d'entrées d'annuaires}$"} },
                      "title": r"$\text{Nombe d'entrées d'annuaire par décennie dans la base Soduco}$" }
                    , **LAYOUT_BASE)
    )
    
    # graph comparing professions between `df_r_base` and `df_s_base`
    # filter to keep only the decades that are in common between the two datasets
    intersect = sorted(list( set(df_s_base.decade).intersection(set(df_r_base.decade)) ))
    df_s_base = df_s_base[ df_s_base.decade.isin(intersect) ]
    df_r_base = df_r_base[ df_r_base.decade.isin(intersect) ]
    
    # create the graphs
    dsdf = { "Soduco": df_s_base, "Richelieu": df_r_base }
    fig_chap = build_figure_comparison(dsdf, "chapeliers")
    fig_nouv = build_figure_comparison(dsdf, "nouveautes")
    fig_arch = build_figure_comparison(dsdf, "architectes")
    
    # show and write
    to_write = { 
        "soduco_nb_entrees_decade": fig_s_decade,
        "richelieu_absolu": fig_r,
        "soduco_absolu": fig_s,
        "richelieu_fraction": fig_r_frac,
        "soduco_fraction": fig_s_frac,
        "richelieu_base_1835": fig_r_base,
        "soduco_base_1835": fig_s_base,
        "chapeliers_base_1835": fig_chap,
        "nouveautes_base_1835": fig_nouv,
        "architectes_base_1835": fig_arch,
    }
    for k,v in to_write.items():
        v.write_image(os.path.join(OUTPLOT, f"{k}.png")
                      , format="png", width=1500, height=1000)
        v.write_html(os.path.join(OUTPLOT, f"{k}.html")
                     , include_plotlyjs="cdn", include_mathjax="cdn")
        v.show()
    
    return


if __name__ == "__main__":
    pipeline()
    
    



