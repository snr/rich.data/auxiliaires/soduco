# SODUCO Séminaire 06.11.2023

Code réalisé pour la conférence: 
> Charlotte Duvette, Paul Kervegan, "Chapeliers, architectes, nouveautés: géo-analyse des activités visibles et invisibles d'un quartier", *Res(t)ituer les annuaires commerciaux et les évolutions de l'espace parisien du XIXe siècle. Partager l'expérience et les ressources du programme ANR SoDUCo*, 6 et 7 novembre 2023, Paris: Bibliothèque nationale de France.

## Pipeline

- réaliser les requêtes SQL présentes dans `sql/` (ce qui demande un accès à la base de données Soduco). les résultats sont enregistrés dans les fichiers CSV correspondant au mot-clé sur lequel la recherche est menée.
- copier les résultats des requêtes en CSV dans le dossier `in/`, avec le fichier `richelieu_icono.csv`, qui décrit les ressources iconographiques du projet Richelieu
- réaliser la chaîne d'analyse Python:
  ```bash
  # chaîne pour linux / macOS
  python3 -m venv env              # créer un environnement virtuel
  source env/bin/activate          # sourcer l'env
  pip install -r requirements.txt  # installer les dépendances
  python main.py                   # lancer la chaîne. les résultats sont stockés dans `out/`
  ```
- les résultats sont enregistrés dans `out/`: `out/data` pour la donnée brute en CSV, 
  `out/plot` pour son interprétation en graphiques, au format HTML et PNG.

## Licence

Le code est diffusé sous license GNU GPL 3.0, les données Richelieu sous CC BY 4.0 et les données Soduco sous Etalab 2.0.
