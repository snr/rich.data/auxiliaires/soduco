# PIPELINE SODUCO DE LIAGE D'ENTITÉS SUR DES GRAPHES GÉOHISTORIQUES

ce dépôt contient de la documentation et une description d'une pipeline
de liage d'entités nommées sur des graphes géohistoriques, afin d'étudier
l'évolution de commerces dans le temps (changement d'addresses, succession...).

voir le **dépôt original de l'atelier [à cette addresse](https://github.com/soduco/atelier_graphes_geohistoriques_annuaires/)**:
tout ce qui est ici en a été adapté. toutes les données viennent du projet SODUCO.

---

## Technologies:
- postgresql et pgAdmin4
- graphDB
- [silk](https://github.com/silk-framework/silk)

---

# Fichiers

- `1_test_keyword_nouveautes.sql`: récupérer toutes les entrées correspondant aux magasins de nouveautés
- `1_test_keyword_nouveautes_georef.sql`: récupérer toutes les entrées correspondant aux magasins de nouveautés **dans le quartier Richelieu seulement**
- `2_insert_new_graph_data_nouveautes.sql`: insérer les données dans la base SQL
- `2_insert_new_graph_data_nouveautes_georef.sql`: pareil, mais avec les données du quartier Richelieu seulement
- `3_silkconfig_liage_label_activity_tokenwise.xml`: linking spec pour Silk en utilisant une Tokenwise, ce qui **prend du temp**
- `3_silkconfig_liage_label_activity_levenshtein`: la même linking spec, mais avec une distance de Levenshtein au lieu d'une Tokenwise, **ce qui est plus rapide**
- `df_nouveautes_triple.ttl`: triplestore de la base de données créée à l'issue de l'étape 1, **sans géoréférencement**.
- `directory_mapping_liage.ttl`: description du graphe de données

---

## Pipeline

on sélectionne un ou plusieurs *mots-clés* sur lesquels mener l'étude: une raison 
sociale ou un type de commerces, dans notre cas les magasins de nouveautés.

### Requêtes SQL

- **tester les mots clés**, avec `1_test_keyword_nouveaute.sql`
- **insérer données correspondant** aux mots clés dans la base SQL `soduco/directories_graph`, avec
  - `2_insert_new_graph_data_nouveaute.sql`: script qui insère les données dans une deuxième BDD
    qui sera ensuite traduite en RDF avec un mapping de données
  - `2_insert_new_graph_data_nouveaute_splitaddr.sql`: la même, mais avec les addresses séparées 
    entre numéro et nom de rue

### Import des données dans GraphDB

- lancer GraphDB
- **créer un dépôt** pour la base de données: `Configurer > Dépôt > SPARQL Virtuel Ontop`. dans le
  menu de configuration: 
  - `Informations de connexion`: choisir le pilote PostgreSQL (à installer si besoin) et mettre
    les bonnes informations de connexion.
  - `Paramètres Ontop > Fichier OBDA ou R2RML`: uploader `directory_mapping_liage.ttl`
- **télécharger les données RDF** en local: cela rend les calculs beaucoup plus rapides en évitant
  d'avoir à faire les requêtes sur la BDD postgres pendant le liage. on fait cela avec la requête
  SPARQL (en replaçant `nomDuGraphe` par le nom que l'on trouve dans la BDD postgres dans la colonne: 
  `soduco > directories_graph > tables > dataset > graph_name`:
  ```sparql
  CONSTRUCT { ?s ?p ?o }
  WHERE {
    GRAPH <http://rdf.geohistoricaldata.org/id/directories/nomDuGraphe>
    { ?s ?p ?o }
  }
  ```
- **télécharger les résultats** de la requête au format `Turtle`
- dans graphDB, faire `Configurer > Dépôts > Nouveau dépôt > Dépôt GraphDB`, le nommer et laisser
  les autres paramètres par défaut
- **réimporter les données** dans graphDB, `Importer > Télécharger des fichiers RDF`, avec comme `base IRI`: 
  `http://rdf.geohistoricaldata.org/id/directories` et comme `Graphes cibles` le `graphe nommé`
  `http://rdf.geohistoricaldata.org/id/directories/nomDuGraphe`

### Liage avec Silk

**attention: il faut avoir `JAVA 8` installé, avec les variables d'environnement `$JAVA_HOME` et `$PATH`**
qui pointent respectivement vers le dossier de Java 8 (`/usr/lib/jvm/<nom du dossier>`) et `$PATH` vers
`/usr/lib/jvm/<nom du dossier>/bin`. voir [ce post](https://stackoverflow.com/a/63040068/17915803)

- graphDB doit toujours tourner
- **lancer `silk-workbench`**: dans le dossier d'installation, faire `bash/silk-workbench/bin/silk-workbench`
- **créer un `New Project`**, le nommer `nomDuProjet`.
- **dans `Datasets`**, créer 3 jeux de données: 
  - un `output`, type `RDF`, format `N-Triples`. indiquer un nom de fichier `*nt`, qui sera dans 
    `~/.silk//workspace/nomDuProjet/resources`
  - un `source`, dataset d'entrée qui pointe vers la base de données GraphDB: le 1er dataset du liage 
    - dans son `Endpoint URI`, mettre le lien obtenu dans `GraphDB > Configurer > nomDuDataSet > <bouton copier l'URL>`
    - dans son `Graph`, mettre `http://rdf.geohistoricaldata.org/id/directories/nomDuGraphe`
  - un `target`, le dataset avec lequel on va lier `source`. il est produit en copiant le graphe `input`: on
    compare `source` avec soi-même
- **dans `Linking Tasks`, créer une `task`**. Dans ses propriétés:
  - Source dataset: `input`, Source type: `<http://rdf.geohistoricaldata.org/def/directory#Entry>`
  - Target dataset: `output`, Target type: `<http://rdf.geohistoricaldata.org/def/directory#Entry>`
  - Output: le fichier `output` 
- **lancer la `Linking task`**: pour chaque propriété à lier du dataset, faire des simplifications et finir par comparer
  les chaînes dans `source` et `target` avec une fonction de comparaison (type Levenshtein ou Tokenwise distance).
- **faire des tests** avec `Execute` et regarder les résultats dans `Evaluate`. déterminer les scores où on est sûr.e.s
  que le liage est bon, et ceux où on est moins sur.e.s.
- une fois qu'on a mis au point sa chaîne, l'adapter en un **fichier de config XML**: ici, `3_silkconfig_liage_label_activity.xml`.
- lancer la linking task en **ligne de commande** avec:
  - `-Xms8g`: RAM minimum, en GB
  - `-Xms10g`: RAM maximum, en GB
  - `-DconfigFile`: fichier de conf du liage
  ```shell
  java -Xms8g -Xmx10g -DconfigFile=3_silkconfig_liage_label_activity.xml -jar silk.jar
  ```

---

## Autres

- Documentation: ontologie du graphe définie dans `directory_mapping_liage.ttl`, peut être ouverte via 
  [**`WebVowl`**](service.tib.eu/webvowl) ou **`Protégé`** (éditeur d'ontologie)



