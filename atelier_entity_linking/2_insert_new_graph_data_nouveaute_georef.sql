INSERT INTO directories_graph.directories_content 
(
	--Pour chaque entrée, compte le nombre de '<PER>' dans la chaîne XML retournée par la pipeline NER
	WITH per_count AS (
		SELECT e.index, (length(e.ner_xml) - length(replace(e.ner_xml, '<PER>', '' ))) / length('<PER>') AS count_
		FROM directories.elements AS e
		ORDER BY count_ DESC
	), short_list AS ( 
    --Ne conserve que les entrées avec 0 ou 1 PER (au delà, on aura un produit cartésien de tous les attributs)
		SELECT pc.index
		FROM per_count AS pc
		WHERE count_ <=1
  )
	SELECT DISTINCT e.index, p.ner_xml AS person, act.ner_xml AS activity, s.loc AS loc, s.cardinal AS cardinal,
	t.ner_xml AS title, e.directory, e.published, lower((COALESCE(s.loc,'') || ' '::text) || COALESCE(s.cardinal,'')) AS fulladd
	FROM short_list AS l
	INNER JOIN directories.elements AS e ON l.index = e.index
	INNER JOIN directories.persons AS p ON e.index = p.entry_id
	INNER JOIN directories.activities AS act ON e.index = act.entry_id
	INNER JOIN directories.addresses AS s ON e.index = s.entry_id
	INNER JOIN directories.titles AS t ON e.index = t.entry_id
	WHERE (
		-- Liste des mots-clés
    (
      act.ner_xml ILIKE '%nouveauté%' 
      OR act.ner_xml ILIKE '%nouveaute%' 
      OR act.ner_xml ILIKE '%nouvaute%' 
      OR act.ner_xml ILIKE '%nouv%' 
    ) AND NOT (
      lower(act.ner_xml) ~ 'nouveau(x|$|(?!\w))'
      OR act.ner_xml ILIKE '%nouvel%'
    )
    AND ST_Within(ST_Transform(ST_GeomFromText(g."precise.geom", 2154),4326), ST_GeomFromText('POLYGON ((2.3351340033659085 48.86403083317913, 2.3325449613491003 48.870086276927026, 2.3328805779065647 48.87027549773694, 2.3328326326835622 48.87071701017666, 2.3400244160660577 48.87197845282293, 2.3430449650873015 48.87156848745147, 2.340935375294606 48.86623863192386, 2.3413668822979616 48.866144013975656, 2.341846334523183 48.86586015905658, 2.341510717965747 48.865481683326834, 2.340791539626821 48.86538706394742, 2.3394970186179194 48.86298997998804, 2.3351340033659085 48.86403083317913))',4326))
  )
	ORDER BY e.index, e.published ASC
);
	
INSERT INTO directories_graph.geocoding 
(
	--Pour chaque entrée, compte le nombre de '<PER>' dans la chaîne XML retournée par la pipeline NER
	WITH per_count AS (
		SELECT e.index, (length(e.ner_xml) - length(replace(e.ner_xml, '<PER>', '' ))) / length('<PER>') AS count_
		FROM directories.elements AS e
		ORDER BY count_ DESC
	), short_list AS ( --Ne conserve que les entrées avec 0 ou 1 PER (au delà, on aura un produit cartésien de tous les attributs)
		SELECT pc.index
		FROM per_count AS pc
		WHERE count_ <=1
  )
	SELECT DISTINCT e.index as entry_id, g.index as id_address, g."precise.geo_response" AS precise_geo_response, ST_Transform(ST_GeomFromText(g."precise.geom",2154),4326) AS precise_geom
	FROM short_list AS l
	INNER JOIN directories.elements AS e ON l.index = e.index
	INNER JOIN directories.activities AS act ON e.index = act.entry_id
	INNER JOIN directories.geocoding AS g ON e.index = g.entry_id
	WHERE (
		(g."precise.geo_response" NOT LIKE '')
		AND (
      -- Liste des mots-clés
      (
        act.ner_xml ILIKE '%nouveauté%' 
        OR act.ner_xml ILIKE '%nouveaute%' 
        OR act.ner_xml ILIKE '%nouvaute%' 
        OR act.ner_xml ILIKE '%nouv%' 
      ) AND NOT (
        lower(act.ner_xml) ~ 'nouveau(x|$|(?!\w))'
        OR act.ner_xml ILIKE '%nouvel%'
      )
    )
    AND ST_Within(ST_Transform(ST_GeomFromText(g."precise.geom", 2154),4326), ST_GeomFromText('POLYGON ((2.3351340033659085 48.86403083317913, 2.3325449613491003 48.870086276927026, 2.3328805779065647 48.87027549773694, 2.3328326326835622 48.87071701017666, 2.3400244160660577 48.87197845282293, 2.3430449650873015 48.87156848745147, 2.340935375294606 48.86623863192386, 2.3413668822979616 48.866144013975656, 2.341846334523183 48.86586015905658, 2.341510717965747 48.865481683326834, 2.340791539626821 48.86538706394742, 2.3394970186179194 48.86298997998804, 2.3351340033659085 48.86403083317913))',4326))
  )
	ORDER BY e.index
);

UPDATE directories_graph.directories_content SET graph_name ='nouveautes_clean' WHERE graph_name ISNULL;
UPDATE directories_graph.geocoding SET graph_name ='nouveautes_clean' WHERE graph_name ISNULL;
INSERT INTO directories_graph.dataset VALUES ('Nouveautés', '2023-10-02', 'nouveautes_clean');


